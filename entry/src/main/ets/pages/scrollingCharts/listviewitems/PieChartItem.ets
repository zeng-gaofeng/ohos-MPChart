/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { PieData } from '@ohos/mpchart';
import { PieChart } from '@ohos/mpchart'
import  {Legend,LegendVerticalAlignment, LegendOrientation, LegendHorizontalAlignment
} from '@ohos/mpchart';
import { MPPointF } from '@ohos/mpchart';

@Component
export default struct PieChartItem {
  @ObjectLink data: PieData
  @State pieModel: PieChart.Model = new PieChart.Model()

  aboutToAppear() {
    this.pieModel.setHoleRadius(75)
      .setTransparentCircleWidth(5)
      .setOffset(new MPPointF(160, 200))
      .setRadius(150)
      .setPieData(this.data)
      .setUsePercentValues(true)

    let l: Legend = this.pieModel.getLegend()
    l.setOrientation(LegendOrientation.VERTICAL)
    l.setVerticalAlignment(LegendVerticalAlignment.TOP);
    l.setHorizontalAlignment(LegendHorizontalAlignment.RIGHT);
    l.setYEntrySpace(10)
    l.setFormToTextSpace(20)
  }

  build() {
    Column() {
      PieChart({
        model: $pieModel
      })
    }.alignItems(HorizontalAlign.Start)
    .height(450)
  }
}

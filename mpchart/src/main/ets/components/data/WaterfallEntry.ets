/*
 * Copyright (C) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { ImagePaint } from './Paint';
import Range from '../highlight/Range';
import EntryOhos from './EntryOhos';


/**
 * Entry class for the BarChart. (especially stacked bars)
 *
 */
export default class WaterfallEntry extends EntryOhos {

  /**
   * the values the stacked barchart holds
   */
  private mYVals: number[] | null = null;

  /**
   * the ranges for the individual stack values - automatically calculated
   */
  private mRanges: Range[] | null = null;

  /**
   * the sum of all negative values this entry (if stacked) contains
   */
  private mNegativeSum: number = 0;

  /**
   * the sum of all positive values this entry (if stacked) contains
   */
  private mPositiveSum: number = 0;
  private mYVals2: number[] | null = null;

  /*constructor(x:number, y:number,  icon:Object*/
  /*Drawable*/
  constructor(x?: number, y?: number[] | number, y2?: number[], icon?: ImagePaint, data?: Object) {
    super(x, y === undefined || y === null ? 0 : typeof y === 'number' ? y : WaterfallEntry.calcSum(y), icon, data);
    if(!!!y){
      y = [0, 0];
    }
    if (typeof y === 'object') {
      this.mYVals = y;
      this.mYVals2 = y2 ?y2:null;
      this.calcPosNegSum();
      this.calcRanges();
    }
  }
  /**
   * Returns an exact copy of the BarEntry.
   */
  public copy(): WaterfallEntry {
    let data:Object|null|undefined = this.getData();
    if (data === null) {
      data = undefined;
    }
    let copied: WaterfallEntry = new WaterfallEntry(this.getX(), this.getY(), undefined, undefined, data);
    if (this.mYVals) {
      copied.setVals(this.mYVals);
    }
    return copied;
  }

  /*
   * Returns the stacked values this BarEntry represents, or null, if only a single value is represented (then, use
   * getY()).
   *
   * @return
   */
  public getYVals(): number[]{
    return this.mYVals!;
  }

  public getYVals2(): number[] {
    return this.mYVals2!;
  }

  /**
   * Set the array of values this BarEntry should represent.
   *
   * @param vals
   */
  public setVals(vals: number[]): void {
    this.setY(WaterfallEntry.calcSum(vals));
    this.mYVals = vals;
    this.calcPosNegSum();
    this.calcRanges();
  }

  /**
   * Returns the value of this BarEntry. If the entry is stacked, it returns the positive sum of all values.
   *
   * @return
   */
  public getY(): number {
    return super.getY();
  }

  /**
   * Returns the ranges of the individual stack-entries. Will return null if this entry is not stacked.
   *
   * @return
   */
  public getRanges(): Range[] {
    return this.mRanges!;
  }

  /**
   * Returns true if this BarEntry is stacked (has a values array), false if not.
   *
   * @return
   */
  public isStacked(): boolean {
    return this.mYVals != null;
  }

  /**
   * Use `getSumBelow(stackIndex)` instead.
   */
  public getBelowSum(stackIndex: number): number {
    return this.getSumBelow(stackIndex);
  }

  public getSumBelow(stackIndex: number): number {
    if (this.mYVals == null)
      return 0;
    let remainder: number = 0;

    for (let i = 0;i < stackIndex; i++) {
      remainder += this.mYVals[i]
    }
    return remainder;
  }

  /**
   * Reuturns the sum of all positive values this entry (if stacked) contains.
   *
   * @return
   */
  public getPositiveSum(): number {
    return this.mPositiveSum;
  }

  /**
   * Returns the sum of all negative values this entry (if stacked) contains. (this is a positive number)
   *
   * @return
   */
  public getNegativeSum(): number {
    return this.mNegativeSum;
  }

  private calcPosNegSum(): void {

    if (this.mYVals == null) {
      this.mNegativeSum = 0;
      this.mPositiveSum = 0;
      return;
    }

    let sumNeg: number = 0;
    let sumPos: number = 0;

    for (let f of this.mYVals) {
      if (f <= 0)
        sumNeg += Math.abs(f);
      else
        sumPos += f;
    }

    this.mNegativeSum = sumNeg;
    this.mPositiveSum = sumPos;
  }

  /**
   * Calculates the sum across all values of the given stack.
   *
   * @param vals
   * @return
   */
  public static calcSum(vals: number[]): number {

    if (vals == null || vals.length == 0)
      return 0;

    return vals[vals.length - 1];
  }

  protected calcRanges(): void {

    let values: number[] | null= this.getYVals();

    if (values == null || values.length == 0)
      return;

    this.mRanges = new Array(values.length);

    let negRemain: number = -this.getNegativeSum();
    let posRemain: number = 0;

    for (let i = 0; i < this.mRanges.length; i++) {

      let value: number = values[i];

      if (value < 0) {
        this.mRanges[i] = new Range(negRemain, negRemain - value);
        negRemain -= value;
      } else {
        this.mRanges[i] = new Range(posRemain, posRemain + value);
        posRemain += value;
      }
    }
  }

  public calcYValsMin(): number {
    let minNum = 0;
    let yValues:number[] | null = this.mYVals;
    if (yValues) {
      for (let item of yValues) {
        minNum = (item > minNum ? minNum : item)
      }
    }
    return minNum;
  }
}



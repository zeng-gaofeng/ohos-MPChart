/*
 * Copyright (C) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import RadarChart from '../charts/RadarChart'
import {JArrayList} from '../utils/JArrayList'
import Highlight from './Highlight'
import PieRadarHighlighter from './PieRadarHighlighter'
import MPPointF from '../utils/MPPointF'
import IDataSet from '../interfaces/datasets/IDataSet'
import EntryOhos from '../data/EntryOhos'
import Utils from '../utils/Utils'

export default class RadarHighlighter extends PieRadarHighlighter<RadarChart> {

    constructor(chart:RadarChart){
      super(chart);
    }

    protected getClosestHighlight(index:number, x:number, y:number):Highlight {

        let highlights:JArrayList<Highlight> = this.getHighlightsAtIndex(index);

        let distanceToCenter:number = this.mChart.distanceToCenter(x, y) / this.mChart.getFactor();

        let closest:Highlight = null;
        let distance:number = Number.MAX_VALUE;

        for (let i = 0; i < highlights.size(); i++) {
            let high:Highlight = highlights.get(i);
            let cdistance:number = Math.abs(high.getY() - distanceToCenter);
            if (cdistance < distance) {
                closest = high;
                distance = cdistance;
            }
        }

        return closest;
    }
    /**
     * Returns an array of Highlight objects for the given index. The Highlight
     * objects give information about the value at the selected index and the
     * DataSet it belongs to. INFORMATION: This method does calculations at
     * runtime. Do not over-use in performance critical situations.
     *
     * @param index
     * @return
     */
    protected getHighlightsAtIndex(index:number):JArrayList<Highlight> {

        this.mHighlightBuffer.clear();

        let phaseX:number = this.mChart.getAnimator().getPhaseX();
        let phaseY:number = this.mChart.getAnimator().getPhaseY();
        let sliceangle:number = this.mChart.getSliceAngle();
        let factor:number = this.mChart.getFactor();

        let pOut:MPPointF = MPPointF.getInstance(0,0);
        for (let i = 0; i < this.mChart.getData().getDataSetCount(); i++) {

            let dataSet:IDataSet<EntryOhos> = this.mChart.getData().getDataSetByIndex(i);

            let entry:EntryOhos = dataSet.getEntryForIndex(index);

            let y:number = (entry.getY() - this.mChart.getYChartMin());

            Utils.getPosition(
            this.mChart.getCenterOffsets(), y * factor * phaseY,
                    sliceangle * index * phaseX + this.mChart.getRotationAngle(), pOut);

            this.mHighlightBuffer.add(new Highlight(index, entry.getY(), pOut.x, pOut.y, i, dataSet.getAxisDependency()));
        }

        return this.mHighlightBuffer;
    }
}

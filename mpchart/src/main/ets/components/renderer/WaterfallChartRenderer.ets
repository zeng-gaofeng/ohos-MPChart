/*
 * Copyright (C) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import YAxis, { AxisDependency } from '../components/YAxis';
import { WaterfallChartModel } from '../charts/WaterfallChart';
import IWaterfallDataSet from '../interfaces/datasets/IWaterfallDataSet';
import Range from '../highlight/Range';
import BarLineScatterCandleBubbleRenderer from './BarLineScatterCandleBubbleRenderer';
import MyRect from '../data/Rect';
import BarBuffer from '../buffer/BarBuffer';
import Paint, { Style, RectPaint, ImagePaint, TextPaint } from '../data/Paint';
import WaterfallData from '../data/WaterfallData';
import WaterfallEntry from '../data/WaterfallEntry';
import Transformer from '../utils/Transformer';
import { MyDirection } from '../utils/Fill';
import MPPointF from '../utils/MPPointF';
import ViewPortHandler from '../utils/ViewPortHandler';
import Utils from '../utils/Utils';
import { JArrayList } from '../utils/JArrayList';
import Highlight from '../highlight/Highlight';
import ChartAnimator from '../animation/ChartAnimator'
import BarEntry from '../data/BarEntry';
import IBarDataSet from '../interfaces/datasets/IBarDataSet';
import IDataSet from '../interfaces/datasets/IDataSet';

export default class WaterfallChartRenderer extends BarLineScatterCandleBubbleRenderer {
  protected mChart: WaterfallChartModel | null = null;

  /**
   * the rect object that is used for drawing the bars
   */
  protected mBarRect: MyRect = new MyRect();
  protected mBarBuffers: BarBuffer[] | null = null;
  protected mShadowPaint: Paint = new RectPaint();
  protected mBarBorderPaint: Paint = new RectPaint();
  private maxTextOffset: number = 0;
  private width: number = 0;
  private height: number = 0;
  private singleWidth: number = 0;
  private marginCount = 12;
  private marginRight = 0;
  private count = 0;

  constructor(chart: WaterfallChartModel, viewPortHandler: ViewPortHandler) {
    super(null, viewPortHandler);
    this.mChart = chart;

    this.mHighlightPaint = new Paint();
    this.mHighlightPaint.setStyle(Style.FILL);
    this.mHighlightPaint.setColor(Color.Black);
    // set alpha after color
    this.mHighlightPaint.setAlpha(120);

    this.mShadowPaint.setStyle(Style.FILL);

    this.mBarBorderPaint.setStyle(Style.STROKE);
  }

  public initBuffers(): void {
    if (this.mChart) {
      let barData: WaterfallData | null = this.mChart.getWaterfallData();
      if (barData) {
        this.mBarBuffers = new Array(barData.getDataSetCount());

        for (let i = 0; i < this.mBarBuffers.length; i++) {
          let dataSet: IWaterfallDataSet | null = barData.getDataSetByIndex(i);
          if (dataSet) {
            this.mBarBuffers[i] = new BarBuffer(dataSet.getEntryCount() * 4 * (dataSet.isStacked() ? dataSet.getStackSize() : 1),
              barData.getDataSetCount(), dataSet.isStacked());
          }
        }
      }
    }
  }

  public drawData(): Paint[] {
    if (!this.mChart) {
      return [];
    }
    this.singleWidth = 0;
    this.marginRight = 0;
    this.count = 0;
    this.width = 0;
    this.height = 0;
    this.maxTextOffset = 0;

    let rectPaintArr: Paint[] = new Array();
    let barData: WaterfallData | null = this.mChart.getWaterfallData();
    if (!barData) {
      return [];
    }
    let leftAxis: YAxis | null = this.mChart.getAxis(AxisDependency.LEFT);
    if (!leftAxis) {
      return [];
    }
    let textPaint: TextPaint = new TextPaint();
    textPaint.setTextSize(leftAxis.getTextSize())
    this.maxTextOffset = Utils.calcTextWidth(textPaint, leftAxis.getAxisMinimum() < 0 ? leftAxis.getAxisMinimum()
                                                                                          .toFixed(0) + "" : leftAxis.getAxisMaximum()
                                                                                                             .toFixed(0) + "");
    if (!this.mViewPortHandler) {
      return []
    }
    let right = this.mViewPortHandler.contentRight() - this.maxTextOffset;
    this.width = right - this.mViewPortHandler.contentLeft() - this.maxTextOffset;
    this.height = this.mViewPortHandler.contentBottom() - this.mViewPortHandler.contentTop();

    for (let i = 0; i < barData.getDataSetCount(); i++) {
      let dataSet: IWaterfallDataSet | null = barData.getDataSetByIndex(i);
      if (dataSet) {
        let entries = dataSet.getEntries();
        if (entries) {
          this.count += entries.size();
        }
      }
    }
    for (let i = 0; i < barData.getDataSetCount(); i++) {
      let dataSet: IWaterfallDataSet | null = barData.getDataSetByIndex(i);

      this.singleWidth = this.width / this.count;
      this.marginRight = this.marginCount / this.count;


      if (dataSet && dataSet.isVisible()) {
        rectPaintArr = rectPaintArr.concat(this.drawDataSet(dataSet, i));
      }
    }
    return rectPaintArr;
  }

  private mBarShadowRectBuffer: MyRect = new MyRect();

  protected drawDataSet(dataSet: IWaterfallDataSet, index: number): Paint[] {
    if (!this.mChart) {
      return [];
    }
    let axisDependency = AxisDependency.LEFT;
    if (dataSet.getAxisDependency()) {
      axisDependency =dataSet.getAxisDependency()
    }
    let trans: Transformer | null = this.mChart.getTransformer(axisDependency);

    this.mBarBorderPaint.setColor(dataSet.getBarBorderColor());
    this.mBarBorderPaint.setDotsColor(dataSet.getDotsColor());
    this.mBarBorderPaint.setStrokeWidth(Utils.convertDpToPixel(dataSet.getBarBorderWidth()));

    let drawBorder: boolean = dataSet.getBarBorderWidth() > 0;

    let phaseX: number = this.mAnimator.getPhaseX();
    let phaseY: number = this.mAnimator.getPhaseY();

    let rectPaintArr: Paint[] = new Array();
    // draw the bar shadow before the values
    if (this.mChart.isDrawBarShadowEnabled()) {
      this.mShadowPaint.setColor(dataSet.getBarShadowColor());

      let barData: WaterfallData | null= this.mChart.getWaterfallData();
      if (!barData) {
        return [];
      }
      let barWidth: number = barData.getBarWidth();
      let barWidthHalf: number = barWidth / 2.0;
      let x: number;
      for (let i = 0, count = Math.min((Math.ceil((dataSet.getEntryCount()) * phaseX)), dataSet.getEntryCount());
           i < count;
           i++) {

        let e = dataSet.getEntryForIndex(i);
        if (e) {
          x = e.getX();

          this.mBarShadowRectBuffer.left = x - barWidthHalf;
          this.mBarShadowRectBuffer.right = x + barWidthHalf;
        }

        if (trans) {
          trans.rectValueToPixel(this.mBarShadowRectBuffer);
        }
        if (!this.mViewPortHandler) {
          return [];
        }
        if (!this.mViewPortHandler.isInBoundsLeft(this.mBarShadowRectBuffer.right))
          continue;

        if (!this.mViewPortHandler.isInBoundsRight(this.mBarShadowRectBuffer.left))
          break;

        this.mBarShadowRectBuffer.top = this.mViewPortHandler.contentTop();
        this.mBarShadowRectBuffer.bottom = this.mViewPortHandler.contentBottom();

        let rectPaint: RectPaint = new RectPaint(this.mShadowPaint as RectPaint);
        let style = this.mShadowPaint.getStyle();
        if (style) {
          rectPaint.setStyle(style);
        }
        rectPaint.setX(this.mBarShadowRectBuffer.left)
        rectPaint.setY(this.mBarShadowRectBuffer.top)
        rectPaint.setWidth(Math.abs(this.mBarShadowRectBuffer.right - this.mBarShadowRectBuffer.left))
        rectPaint.setHeight(Math.abs(this.mBarShadowRectBuffer.bottom - this.mBarShadowRectBuffer.top))
        rectPaintArr.push(rectPaint)
      }
    }

    // initialize the buffer
    if (!this.mBarBuffers) {
      return [];
    }
    let buffer: BarBuffer = this.mBarBuffers[index];
    buffer.setPhases(phaseX, phaseY);
    buffer.setDataSet(index);

    buffer.setInverted(this.mChart.isInverted(axisDependency));
    let waterfallData = this.mChart.getWaterfallData();
    if (waterfallData) {
      buffer.setBarWidth(waterfallData.getBarWidth());
    }

    buffer.feed(dataSet);
    if (trans) {
      trans.pointValuesToPixel(buffer.buffer);
    }

    let isCustomFill: boolean = false;
    let dataFills = dataSet.getFills();
    if (dataFills) {
      isCustomFill = dataFills && !dataFills.isEmpty();
    }
    let isSingleColor: boolean = dataSet.getColors().size() == 1;
    let isInverted: boolean = this.mChart.isInverted(axisDependency);

    if (isSingleColor) {
      this.mRenderPaint.setColor(dataSet.getColor());
    }

    let position: number = 0;
    let number = 0;
    let yValPosition = 0;
    for (let i = 0; i < index; i++) {

      let dataSet: IWaterfallDataSet | null= this.mChart.getWaterfallData()!.getDataSetByIndex(index - 1);

      if (dataSet) {
        let entries = dataSet.getEntries();
        if (entries) {
          number += entries.size();
        }
      }
    }
    for (let j = 0, pos = 0; j < buffer.size(); j += 4, pos++) {

      let entries = dataSet.getEntries();
      if (!entries) {
      return rectPaintArr;
      }
      let waterFallEntry: WaterfallEntry = entries.get(position) as WaterfallEntry;

      let yLength:number = 0;
      let axis = this.mChart.getAxis(AxisDependency.LEFT);
      if (axis) {
        yLength = axis.getAxisMaximum() - axis.getAxisMinimum();
      }

      let yAxisMinValueAbs:number = 0 //Math.abs(this.mChart.getAxis(AxisDependency.LEFT).getAxisMinimum()) / yLength  * this.height;
      let positionY = waterFallEntry.getY() > 0 ? 0 : Math.abs(waterFallEntry.getY()) / yLength * this.height

      buffer.buffer[j] = this.singleWidth * number + this.mChart.finalViewPortHandler.offsetLeft() + this.maxTextOffset + this.marginRight
      buffer.buffer[j + 1] = this.mChart.finalViewPortHandler.contentBottom() - yAxisMinValueAbs - (waterFallEntry.getY() > 0 ? (waterFallEntry.getY() / yLength * this.height) : 0);
      buffer.buffer[j + 2] = this.singleWidth * (number + 1) + this.mChart.finalViewPortHandler.offsetLeft() + this.maxTextOffset - this.marginRight
      buffer.buffer[j + 3] = this.mChart.finalViewPortHandler.contentBottom() - yAxisMinValueAbs + positionY;
      let val = waterFallEntry.getY();
      let waterFallYValues = waterFallEntry.getYVals();
      if (dataSet.isStacked() &&  waterFallYValues && waterFallYValues.length > 0) {
        let height = buffer.buffer[j + 3] - buffer.buffer[j + 1];
        val = waterFallYValues[yValPosition];
        if (yValPosition == 0) {
          buffer.buffer[j + 3] = this.mChart.finalViewPortHandler.contentBottom();
        } else {
          val = waterFallEntry.getSumBelow(yValPosition);
          buffer.buffer[j + 3] = this.mChart.finalViewPortHandler.contentBottom() - val / waterFallEntry.getY() * height;
        }
        if (yValPosition == waterFallYValues.length - 1) {
          yValPosition = 0;
          position++;
          number++;
        } else {
          val = waterFallEntry.getSumBelow(yValPosition + 1);
          buffer.buffer[j + 1] = this.mChart.finalViewPortHandler.contentBottom() - val / waterFallEntry.getY() * height;
          yValPosition++;
        }
      } else {

        position++;
        number++;
      }
      if (!this.mViewPortHandler) {
        break;
      }
      if (!this.mViewPortHandler.isInBoundsLeft(buffer.buffer[j + 2]))
        continue;

      if (!this.mViewPortHandler.isInBoundsRight(buffer.buffer[j]))
        break;

      if (!isSingleColor) {
        // Set the color for the currently drawn value. If the index
        // is out of bounds, reuse colors.
        this.mRenderPaint.setColor(dataSet.getColor(pos));
      }

      if (isCustomFill) {
        let rectPaint: RectPaint = new RectPaint();
        let style = this.mRenderPaint.getStyle();
        if (style) {
          rectPaint.setStyle(style)
        }
        rectPaint.setColor(this.mRenderPaint.getColor())
        let dataFill =  dataSet.getFill(pos)
        if (dataFill) {
          let paint = dataFill.fillRect(
            rectPaint,
            buffer.buffer[j],
            buffer.buffer[j + 1],
            buffer.buffer[j + 2],
            buffer.buffer[j + 3],
            isInverted ? MyDirection.UP : MyDirection.DOWN);
          if (paint) {
            paint.setStroke(this.mBarBorderPaint.getColor())
            paint.setStrokeWidth(this.mBarBorderPaint.getStrokeWidth())
            paint.value = val;
            rectPaintArr.push(paint);
          }
        }
      } else {
        let rectPaint: RectPaint = new RectPaint();
        let paintX = buffer.buffer[j]
        if (this.mChart.cylinderCenter) {
          if (this.mChart.cylinderWidth !== 0) {
            paintX = buffer.buffer[j] + (Math.abs(buffer.buffer[j + 2] - buffer.buffer[j]) / 2 - this.mChart.cylinderWidth / 2)
          }
        }
        let painty = buffer.buffer[j+1]
        let paintWidth = (this.mChart.cylinderWidth !== 0) && (this.mChart.cylinderWidth < Math.abs(buffer.buffer[j + 2] - buffer.buffer[j])) ? this.mChart.cylinderWidth : Math.abs(buffer.buffer[j + 2] - buffer.buffer[j]);
        let paintHeight = Math.abs(buffer.buffer[j + 3] - buffer.buffer[j + 1])
       let style = this.mRenderPaint.getStyle();
        if (style) {
          rectPaint.setStyle(style)
        }
        rectPaint.setColor(this.mRenderPaint.getColor())
        rectPaint.setX(paintX)
        rectPaint.setY(painty)
        rectPaint.setWidth(paintWidth)
        rectPaint.setDotsWidth(paintWidth)
        rectPaint.setHeight(paintHeight)
        rectPaint.setStroke(this.mBarBorderPaint.getColor())
        rectPaint.setStrokeWidth(this.mBarBorderPaint.getStrokeWidth())

        if(paintWidth / 2 < this.mChart.radius){
          this.mChart.radius = paintWidth / 2;
        }
        let waterFallYValues = waterFallEntry.getYVals();
        let waterFallYValues2 = waterFallEntry.getYVals2();
        if ( waterFallYValues && waterFallYValues2 && waterFallYValues2.length > 0 && waterFallYValues2.length <= 2) {
          if (waterFallYValues2.length < 2) {
            if (waterFallYValues2[0] >= waterFallYValues[0] && waterFallYValues2[0] <= waterFallYValues[1]) {
              rectPaint.setDotsVisibility(Visibility.Visible)
              rectPaint.setDotsHeight(paintWidth)
              if (waterFallYValues[1] === waterFallYValues2[0]) {
                rectPaint.setDotsPosition(painty)
              } else if (waterFallYValues[0] === waterFallYValues2[0]) {
                rectPaint.setDotsPosition(painty + paintHeight - paintWidth)
              } else {
                let yHeight = (waterFallYValues[1] - waterFallYValues[0]) / 2
                let percentage = paintHeight * ((waterFallYValues[1] - waterFallYValues2[0]) / (waterFallYValues[1] - waterFallYValues[0]))
                if (waterFallYValues2[0] > (yHeight + waterFallYValues[0])) {
                  rectPaint.setDotsPosition(painty + percentage)
                } else {
                  rectPaint.setDotsPosition(painty + percentage - paintWidth)
                }
              }
            }
          } else {
            if (waterFallYValues2[0] >= waterFallYValues[0] && waterFallYValues2[0] <= waterFallYValues[1]
            && waterFallYValues2[1] >= waterFallYValues[0] && waterFallYValues2[1] <= waterFallYValues[1]) {
              rectPaint.setDotsVisibility(Visibility.Visible)
              if (waterFallYValues2[1] === waterFallYValues[1]) {
                let percentage = paintHeight * ((waterFallYValues[1] - waterFallYValues2[0]) / (waterFallYValues[1] - waterFallYValues[0]))
                let percentage2 = paintHeight * ((waterFallYValues2[1] - waterFallYValues2[0]) / (waterFallYValues[1] - waterFallYValues[0]))
                let percentageRadius = paintHeight * ((this.mChart.radius / 4) / (waterFallYValues[1] - waterFallYValues[0]))
                rectPaint.setDotsPosition(painty)
                if (percentage2 < percentageRadius && percentageRadius > percentage) {
                  rectPaint.setDotsHeight(paintWidth)
                } else {
                  rectPaint.setDotsHeight(percentage)
                }
              } else if (waterFallYValues2[0] === waterFallYValues[0]) {
                let percentage1 = paintHeight * ((waterFallYValues[1] - waterFallYValues2[1]) / (waterFallYValues[1] - waterFallYValues[0]))
                let percentage2 = paintHeight * ((waterFallYValues2[1] - waterFallYValues2[0]) / (waterFallYValues[1] - waterFallYValues[0]))
                let percentage3 = paintHeight * ((waterFallYValues2[1] - waterFallYValues[0]) / (waterFallYValues[1] - waterFallYValues[0]))
                let percentageRadius = paintHeight * ((this.mChart.radius / 4) / (waterFallYValues[1] - waterFallYValues[0]))
                if (percentage2 < percentageRadius && percentageRadius > percentage3) {
                  rectPaint.setDotsPosition(painty + paintHeight - paintWidth)
                  rectPaint.setDotsHeight(paintWidth)
                } else {
                  rectPaint.setDotsPosition(painty + percentage1)
                  rectPaint.setDotsHeight(paintHeight - percentage1)
                }
              } else {
                let percentageRadius = paintHeight * ((this.mChart.radius / 3) / (waterFallYValues[1] - waterFallYValues[0]))
                let percentage = paintHeight * ((waterFallYValues2[1] - waterFallYValues2[0]) / (waterFallYValues[1] - waterFallYValues[0]))
                if (percentage < percentageRadius) {
                  if ((waterFallYValues[0] + waterFallYValues[1]) / 2 < waterFallYValues2[1]) {
                    let percentageTop = paintHeight * ((waterFallYValues[1] - waterFallYValues2[1]) / (waterFallYValues[1] - waterFallYValues[0]))
                    rectPaint.setDotsHeight(paintWidth)
                    rectPaint.setDotsPosition(painty + percentageTop)
                  } else {
                    let percentageBottm = paintHeight * ((waterFallYValues2[0] - waterFallYValues[0]) / (waterFallYValues[1] - waterFallYValues[0]))
                    rectPaint.setDotsHeight(paintWidth)
                    rectPaint.setDotsPosition(painty + paintHeight - percentageBottm - paintWidth)
                  }
                } else {
                  let percentage = paintHeight * ((waterFallYValues2[1] - waterFallYValues2[0]) / (waterFallYValues[1] - waterFallYValues[0]))
                  rectPaint.setDotsHeight(percentage)
                  let percentagePosition = paintHeight * ((waterFallYValues[1] - waterFallYValues2[1]) / (waterFallYValues[1] - waterFallYValues[0]))
                  rectPaint.setDotsPosition(painty + percentagePosition)
                }
              }
            }
          }
        }
        if (this.mBarBorderPaint.getDotsColor() !== undefined) {
          rectPaint.setDotsColor(this.mBarBorderPaint.getDotsColor())
        }
        if (this.mChart.cylinderCenter && this.mChart.mXAxis) {
          this.mChart.mXAxis.setXCenterOffset(Math.abs(buffer.buffer[j + 2] - buffer.buffer[j]) / 2)
        }
        if (this.mChart.radius) {
          rectPaint.setRadius(this.mChart.radius)
        }
        rectPaint.value = val;
        rectPaintArr.push(rectPaint)
      }
    }
    return rectPaintArr
  }

  protected prepareBarHighlight(x: number, y1: number, y2: number, barWidthHalf: number, trans: Transformer): void {

    let left: number = x - barWidthHalf;
    let right: number = x + barWidthHalf;
    let top: number = y1;
    let bottom: number = y2;

    this.mBarRect.set(left, top, right, bottom);

    trans.rectToPixelPhase(this.mBarRect, this.mAnimator.getPhaseY());
  }

  public drawValues(): Paint[] {
    if (!this.mChart) {
      return [];
    }
    let paintArr: Paint[] = [];
    // if values are drawn
    if (this.mChart.isDrawingValuesAllowed()) {
      let waterFallEntry =this.mChart.getWaterfallData();
      if (!waterFallEntry) {
        return [];
      }
      let dataSets: JArrayList<IWaterfallDataSet> | null = waterFallEntry.getDataSets();
      if (!dataSets){
        return [];
      }
      let valueOffsetPlus: number = 4.5;
      let posOffset: number = 0;
      let negOffset: number = 0;
      let drawValueAboveBar: boolean = this.mChart.isDrawValueAboveBarEnabled();


      for (let i = 0; i < waterFallEntry.getDataSetCount(); i++) {

        let dataSet: IWaterfallDataSet | null= dataSets.get(i);
        if (!dataSet) {
          break;
        }
        if (!this.shouldDrawValues(dataSet))
          continue;

        // apply the text-styling defined by the DataSet
        this.applyValueTextStyle(dataSet);
        // get the buffer
        if (!this.mBarBuffers) {
          return [];
        }
        let buffer: BarBuffer = this.mBarBuffers[i];

        let phaseY: number = this.mAnimator.getPhaseY();
        let iconsOffset: MPPointF = MPPointF.getInstance(undefined, undefined, dataSet.getIconsOffset());
        if (iconsOffset.x != undefined) {
          iconsOffset.x = Utils.convertDpToPixel(iconsOffset.x);
        }
        if (iconsOffset.y != undefined) {
          iconsOffset.y = Utils.convertDpToPixel(iconsOffset.y);
        }
        let position: number = 0;
        let number = 0;
        let yValPosition: number = 0;
        for (let j = 0; j < i; j++) {

          let dataSet: IWaterfallDataSet | null = waterFallEntry.getDataSetByIndex(i - 1);

          if (dataSet) {
            let entries = dataSet.getEntries();
            if (entries) {
              number += entries.size();
            }
          }
        }
        for (let j = 0, pos = 0; j < buffer.size(); j += 4, pos++) {
          let dataEntries = dataSet.getEntries();
          if (!dataEntries) {
          break;
          }
          let barEntry: WaterfallEntry = dataEntries.get(position) as WaterfallEntry;

          let yLength :number= 0;
          let axis = this.mChart.getAxis(AxisDependency.LEFT);
          if (axis) {
            yLength = axis.getAxisMaximum() - axis.getAxisMinimum();
          }
          yLength = yLength === 0 ? 1 : yLength
          this.height = this.height === 0 ? 1 : this.height

          let yAxisMinValueAbs :number= 0;
          if (axis) {
            yAxisMinValueAbs = Math.abs(axis.getAxisMinimum()) / yLength * this.height;
          }
          let positionY = barEntry.getY() > 0 ? 0 : Math.abs(barEntry.getY()) / yLength * this.height
          if (this.mViewPortHandler) {
            buffer.buffer[j] = this.singleWidth * number + this.mViewPortHandler.offsetLeft() + this.maxTextOffset + this.marginRight
            buffer.buffer[j + 1] = this.mChart.finalViewPortHandler.contentBottom() - yAxisMinValueAbs - (barEntry.getY() > 0 ? (barEntry.getY() / yLength * this.height) : -positionY);
            buffer.buffer[j + 2] = this.singleWidth * (number + 1) + this.mViewPortHandler.offsetLeft() + this.maxTextOffset - this.marginRight
            buffer.buffer[j + 3] = this.mChart.finalViewPortHandler.contentBottom() - yAxisMinValueAbs + positionY;
          }
          if (dataSet.isStacked()) {
            let height = buffer.buffer[j + 3] - buffer.buffer[j + 1];
            let barEntryYValues = barEntry.getYVals();
            if (barEntryYValues) {
              let val = barEntryYValues[yValPosition];
              if (yValPosition == 0) {
                buffer.buffer[j + 3] = this.mChart.finalViewPortHandler.contentBottom();
              } else {
                val = barEntry.getSumBelow(yValPosition);
                buffer.buffer[j + 3] = this.mChart.finalViewPortHandler.contentBottom() - val / barEntry.getY() * height;
              }
              if (yValPosition == barEntryYValues.length - 1) {
                yValPosition = 0;
                position++;
                number++;
              } else {
                val = barEntry.getSumBelow(yValPosition + 1);
                buffer.buffer[j + 1] = this.mChart.finalViewPortHandler.contentBottom() - val / barEntry.getY() * height;
                yValPosition++;
              }
            } else {

              position++;
              number++;
            }
            }

          let x: number = (buffer.buffer[j] + buffer.buffer[j + 2]) / 2;

          let val: number = barEntry.getY();
          if (axis){
            if (Utils.calcTextWidth(this.mValuePaint, String(axis
              .getAxisMaximum()
              .toFixed(1)).replace(".", "")) > (this.singleWidth * 1.5)) {
              this.mValuePaint.setTextSize(Utils.convertDpToPixel(this.singleWidth / String(axis
                .getAxisMaximum()
                .toFixed(1)).replace(".", "").length));
            }
          }

          let axisDependency = AxisDependency.LEFT;
          if (dataSet.getAxisDependency()){
            axisDependency =dataSet.getAxisDependency();
          }
          let isInverted: boolean = this.mChart.isInverted(axisDependency);
          // calculate the correct offset depending on the draw position of
          // the value
          let valueTextWidth = Utils.calcTextWidth(this.mValuePaint, String(val.toFixed(1)));
          let valueTextHeight: number = Utils.calcTextHeight(this.mValuePaint, "8");
          posOffset = (drawValueAboveBar ? -valueOffsetPlus : valueTextHeight + valueOffsetPlus);
          negOffset = (drawValueAboveBar ? valueTextHeight + valueOffsetPlus : -valueOffsetPlus);
          if (isInverted) {
            posOffset = -posOffset - valueTextHeight;
            negOffset = -negOffset - valueTextHeight;
          }

          posOffset = (this.singleWidth / 2) - valueTextWidth / 2;
          if (dataSet.isStacked()) {
            let barEntryYValues  = barEntry.getYVals()
            if (barEntryYValues) {
              val = barEntryYValues[yValPosition]
              negOffset -= valueTextHeight + valueOffsetPlus;
              dataSet.setValueTextColor(0xffffff)
            }
          }
          if (dataSet.isDrawValuesEnabled()) {
            let dataFormatter = dataSet.getValueFormatter();
            if (dataFormatter) {
              paintArr = paintArr.concat(this.drawValue(dataFormatter, val, barEntry, i, buffer.buffer[j] + posOffset,
                buffer.buffer[j + 1] - (barEntry.getY() > 0 ? negOffset : -valueOffsetPlus),
                dataSet.getValueTextColor(j / 4)));
            }
          }

          if (barEntry.getIcon() != null && dataSet.isDrawIconsEnabled()) {

            let icon: ImagePaint | null = barEntry.getIcon();
            if (icon) {
              let px: number = x;
              let py: number = val >= 0 ?
                (buffer.buffer[j + 1] + posOffset) :
                (buffer.buffer[j + 3] + negOffset);

              px += iconsOffset.x;
              py += iconsOffset.y;

              paintArr = paintArr.concat(Utils.drawImage(
                icon.getIcon(),
                px,
                py,
                Number(icon.getWidth()),
                Number(icon.getHeight())));
            }
          }
        }


        MPPointF.recycleInstance(iconsOffset);
      }
    }
    return paintArr;
  }

  public drawHighlighted(indices: Highlight[]): Paint[] {
    if (!this.mChart) {
      return [];
    }
    let rectPaintArr: RectPaint[] = new Array();
    let barData: WaterfallData | null= this.mChart.getWaterfallData();
    if (!barData) {
      return [];
    }
    for (let high of indices) {

      let dataSet: IWaterfallDataSet | null = barData.getDataSetByIndex(high.getDataSetIndex());

      if (dataSet == null || !dataSet.isHighlightEnabled())
        continue;

      let e = dataSet.getEntryForXValue(high.getX(), high.getY());
      if (!e) {
        break;
      }
      if (!this.isInBoundsX(e, dataSet))
        continue;
      let axisDependency = AxisDependency.LEFT;
      if (dataSet.getAxisDependency()) {
        axisDependency = dataSet.getAxisDependency();
      }
      let trans: Transformer | null = this.mChart.getTransformer(axisDependency);

      this.mHighlightPaint.setColor(dataSet.getHighLightColor());
      this.mHighlightPaint.setAlpha(dataSet.getHighLightAlpha());

      let isStack: boolean = (high.getStackIndex() >= 0 && (e as WaterfallEntry).isStacked()) ? true : false;

      let y1: number = 0;
      let y2: number = 0;

      if (isStack) {

        if (this.mChart.isHighlightFullBarEnabled()) {

          y1 = (e as WaterfallEntry).getPositiveSum();
          y2 = -(e as WaterfallEntry).getNegativeSum();

        } else {
          let rangeArray = (e as WaterfallEntry).getRanges();
          if (rangeArray) {
            let range: Range = rangeArray[high.getStackIndex()];

            y1 = range.myfrom;
            y2 = range.to;
          }

        }

      } else {
        y1 = e.getY();
        y2 = 0.;
      }
      if (trans) {
        this.prepareBarHighlight(e.getX(), y1, y2, barData.getBarWidth() / 2, trans);
      }
      this.setHighlightDrawPos(high, this.mBarRect);
      let rectPaint: RectPaint = new RectPaint();
      let style = this.mHighlightPaint.getStyle();
      if (style) {
        rectPaint.setStyle(style);
      }
      rectPaint.setStrokeWidth(this.mHighlightPaint.getStrokeWidth());
      rectPaint.setColor(this.mHighlightPaint.getColor());
      rectPaint.setAlpha(this.mHighlightPaint.getAlpha());
      rectPaint.setX(this.mBarRect.left)
      rectPaint.setY(this.mBarRect.top)
      rectPaint.setWidth(this.mBarRect.right - this.mBarRect.left)
      rectPaint.setHeight(this.mBarRect.bottom - this.mBarRect.top)
      rectPaintArr.push(rectPaint);
    }
    return rectPaintArr
  }

  /**
   * Sets the drawing position of the highlight object based on the riven bar-rect.
   * @param high
   */
  protected setHighlightDrawPos(high: Highlight, bar: MyRect): void {
    high.setDraw(bar.centerX(), bar.top);
  }

  public drawExtras(): Paint[] |null {
    return null;
  }

  public getMarginRight(): number {
    return this.marginRight;
  }

  public drawClicked(paint: Paint, paint2: TextPaint, count: number, position: number): Paint[] {
    if (!this.mChart) {
      return [];
    }
    let waterfallData = this.mChart.getWaterfallData();
    if (!waterfallData) {
      return [];
    }
    let rectPaint = new RectPaint();
    rectPaint.set(paint);
    rectPaint.setGradientFillColor([[0x80000000, 1], [0x80000000, 1]]);
    rectPaint.setClickPosition(position)
    let dataSets: JArrayList<IWaterfallDataSet> = waterfallData.getDataSets();
    if (dataSets.length() === 0) {
      return []
    }
    let dataSet: IWaterfallDataSet = dataSets.get(0);
    if (!dataSet.isDrawValuesEnabled() || dataSet.isStacked() || !this.mChart.mDrawClickText) {
      return [rectPaint];
    }

    let textPaint = new TextPaint(paint2);

    let xValue :number= 0;
    let xAxis = this.mChart.getXAxis();
    if (xAxis) {
      xValue = xAxis.getAxisMaximum() / count * (position + 1)
    }
    textPaint.setText("x:" + xValue + ",y:" + textPaint.text)

    textPaint.setX((paint.x + Number(paint.width) / 2) - (Utils.calcTextWidth(this.mValuePaint, textPaint.text) / 2));
    let imagePaint = new ImagePaint();
    imagePaint.setWidth(Utils.calcTextWidth(textPaint, textPaint.text) + 20);
    imagePaint.setHeight(Utils.calcTextHeight(textPaint, textPaint.text) * 4);
    imagePaint.setIcon("app.media.marker2")
    let imageOffsetY = (Utils.calcTextHeight(textPaint, textPaint.text) / 2) + (Number(imagePaint.getHeight()) / 2);
    imagePaint.setX((paint.x + Number(paint.width) / 2) - (Number(imagePaint.width) / 2));
    imagePaint.setY(paint2.getY() - imageOffsetY);
    textPaint.setY(imagePaint.y + (Number(imagePaint.height) / 2) - Utils.calcTextHeight(textPaint, textPaint.text.replace(".", "")))
    textPaint.setColor("#ffffff");
    return [rectPaint, imagePaint, textPaint];
  }
}
